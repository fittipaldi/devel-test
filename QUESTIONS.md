## Referências Técnicas

1. Quais foram os últimos dois livros técnicos que você leu?
R. Introdução a programação com Python - Novatec
   A Arte de SEO - Novatec

2. Quais foram os últimos dois framework/CMS que você trabalhou?
R. Zend e Laravel

3. Descreva os principais pontos positivos do seu framework favorito.
R. Laravel, Bastante Pratico para desenvolvimento de aplicação e tem uma curva de apredizagem fantastica

4. Descreva os principais pontos negativos do seu framework favorito.
R. Laravel, Não ser um framework robusto com suporte a varias funcionalidades nativamente

5. O que é código de qualidade para você.
R. É ter um codigo legivel para ajudar na manutenção, ser peformatico ao maximo, seguir padões, identado

## Conhecimento Linux

1. O que é software livre?
R. É livre para quem quiser altera-lo da forma que desejar sem prejuiso juridico

2. Qual o seu sistema operacional favorito?
R. Para trabalho eu gosto do MAC OS

3. Já trabalhou com Linux ou outro Unix-like?
R. Sim, passei um bom tempo

4. O que é SSH?
R. Protocolo de acesso remoto a um computador (servidor) com sistema operacional Unix via comando

5. Quais as principais diferenças entre sistemas *nix e o Windows?
R. Os Linux tem codigo aberto, já o windows não e a utilização de comandos via terminal que no linux existe uma utilização bem avançada em comparação com o windows

## Conhecimento de desenvolvimento

1. O que é GIT?
R. Sistema de repositorio de arquivos com  controle de versão

2. Descreva um workflow simples de trabalho utilizando GIT.
R. É um ponto de versão valido do sistema, tipo quando temos um sistema que vive sobrendo ajustes podemos ter as versões de referencia e essas verões podemos chamar de workflow

3. O que é PHP Data Objects?
R. PDO é usado diretamente para a manipulação de banco de dados de um forma mais simplificada e o PDO tem suporte a multiplos bancos

4. O que é Database Abstract Layer?
R. É a camada de abstração de banco, com ela o sistema não precisa de se preoculpar com qual o tipo de banco(SGDB) ira utilizar.

5. Você sabe o que é Object Relational Mapping? Se sim, explique.
R. Sim, ORM é a forma de trabalha com um banco relacional em forma de objetos simplificando a manipulação dos dados

6. Como você avalia seu grau de conhecimento em Orientação a objeto?
R. Avançado

7. O que é Dependency Injection?
R. É um padrão de projeto, onde o sistema se torma modularizado tonando a aplicação bem mais configuravel a cada necessidade

8. O significa a sigla S.O.L.I.D?
R. Eu é uma regrada de desenvolvimento que cada letra dessa representa uma normalização de codificação.

9. Qual a finalidade do framework PHPUnit?
R. Aplicação de teste unitado no sistema

10. Explique o que é MVC.
R. Estrutura onde temos o Model, responsavem pela logica do sistema, Controller, responsavel por contrar as ações e View responsavel pela impresão dos dados