<?php
// Define a leitra dos arquivos
define('_EXEC', 1);

session_start();

define('DS', DIRECTORY_SEPARATOR);
define('PATH_ROOT', dirname(__FILE__));

require_once PATH_ROOT . DS . 'controller' . DS . 'Controller.php';

$controller = new Controller;
$html = '';

if (isset($_GET['action'])) {
    $action = $_GET['action'];
} else {
    $action = 'list';
}

switch (strtolower($action)) {

    case 'delete':
        $html .= $controller->deleteAction();
        break;

    case 'save':
        $html .= $controller->saveAction();
        break;

    case 'add':
        $html .= $controller->addAction();
        break;

    case 'edit':
        $html .= $controller->editAction();
        break;

    case'list':
    default:
        $html .= $controller->listAction();
        break;


}
echo $html;