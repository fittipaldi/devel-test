<?php
// no direct access
defined('_EXEC') or die('Acesso Restrito');

require_once PATH_ROOT . DS . 'lib' . DS . 'Db' . DS . 'Db.php';

/**
 * Class Model para gerenciar acesso a Banco de Dados
 */
class Model
{
    /**
     * Pega todos os items
     *
     * @return mixed
     */
    public function getAll()
    {
        /**
         * @var $db Db_Mysql
         */
        $db = new Db;
        $db->db->setQuery('SELECT * FROM cadastros');
        return $db->db->loadObjectList();

    }

    /**
     * Pega um item por ID
     *
     * @param $id
     * @return null
     */
    public function getItem($id)
    {
        $db = new Db;
        $db->db->setQuery('SELECT * FROM cadastros WHERE id = ' . (int)$id);
        $item = $db->db->loadObjectList();
        if (is_array($item))
            return $item[0];
        return null;
    }

    /**
     * Persiste dados no banco
     *
     * @param $data
     * @return bool
     */
    public function saveItem($data)
    {
        $db = new Db;
        $date = new DateTime('now');
        $date = $date->format('Y-m-d H:i:s');

        foreach ($data as $k => $d) {
            $data[$k] = addslashes($d);
        }

        if (!$data['id']) {
            $db->db->setQuery("
               INSERT INTO
               cadastros
               (id, title, slug, description, body, author, insert_date, update_date)
               VALUES
               (null, '{$data['title']}', '{$data['slug']}', '{$data['description']}', '{$data['body']}', '{$data['author']}', '{$date}', '{$date}');
            ");
        } else {
            $db->db->setQuery("
               UPDATE
               cadastros
               SET
               title = '{$data['title']}',
               slug = '{$data['slug']}',
               description = '{$data['description']}',
               body = '{$data['body']}',
               author = '{$data['author']}',
               update_date = '{$date}'
               WHERE
               id = {$data['id']}
            ");
        }

        if ($db->db->query())
            return true;
        return false;
    }

    /**
     * Deleta um item atraves por ID
     *
     * @param $id
     * @return bool
     */
    public function delete($id)
    {
        $db = new Db;
        $db->db->setQuery('DELETE FROM cadastros WHERE id = ' . (int)$id);
        if ($db->db->query())
            return true;
        return false;

    }

}