<?php
// no direct access
defined('_EXEC') or die('Acesso Restrito');

require_once PATH_ROOT . DS . 'config.php';
require_once PATH_ROOT . DS . 'lib' . DS . 'Db' . DS . 'Drive' . DS . 'Mysql.php';

/**
 * Class Db para gerenciamento de SGDB
 */
class Db extends Config
{
    public $db = null;

    /**
     * Instancia o SGDB
     *
     * @return Db_Mysql
     */
    public function __construct()
    {
        switch (strtolower($this->drive)) {
            case 'mysql';
            default:
                $this->db = new Db_Mysql;
                break;
        }
    }

}