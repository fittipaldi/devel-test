<?php
// no direct access
defined('_EXEC') or die('Acesso Restrito');

class Db_Mysql extends Config
{
    private $_query = null;
    private $link = null;
    private $db_selected = null;
    private $result = null;

    public function __construct($user = null, $pass = null, $db = null, $prefix = null, $host = null)
    {
        if ($user) {
            $this->user_db = $user;
        }
        if ($pass) {
            $this->pass_db = $pass;
        }
        if ($db) {
            $this->db = $db;
        }
        if ($prefix) {
            $this->_prefix = $prefix;
        }
        if ($host) {
            $this->host = $host;
        }
    }

    public function setQuery($query, $iniLimit = '', $endLimit = '', $prefix = '#__')
    {
        if (preg_match("/$prefix/i", $query)) {
            $query = preg_replace("/$prefix/i", $this->_prefix, $query);
        }
        if ($endLimit) {
            if ($iniLimit) {
                $query .= " LIMIT {$iniLimit}, {$endLimit} ";
            } else {
                $query .= " LIMIT 0, {$endLimit} ";
            }
        }
        $this->_query = $query;
    }

    public function getQuery()
    {
        return $this->_query;
    }

    private function conect()
    {
        $this->link = mysql_connect($this->host, $this->user_db, $this->pass_db);
        if (!$this->link)
            die('Não foi possível conectar: ' . mysql_error());
    }

    public function close()
    {
        return mysql_close($this->link);
    }

    private function selectTable()
    {
        $this->db_selected = mysql_select_db($this->db, $this->link);
        if (!$this->db_selected)
            die('Não foi possível ao banco:' . $this->db . ' ' . mysql_error());
    }

    public function getAffectRows()
    {
        return mysql_affected_rows();
    }

    public function query()
    {
        $this->conect();
        $this->selectTable();
        $this->result = mysql_query($this->_query);
        $this->close();
        return $this->result;
    }

    private function queryInt()
    {
        $this->conect();
        $this->selectTable();
        $this->result = mysql_query($this->_query);
    }

    public function loadArrayList()
    {
        $this->queryInt();
        $rows = array();
        $i = 0;
        while ($row = mysql_fetch_array($this->result, MYSQL_ASSOC)) {
            $rows[$i] = $row;
            $i++;
        }
        return $rows;
    }

    public function loadObjectList()
    {
        $this->queryInt();
        $rows = array();
        $i = 0;
        while ($row = mysql_fetch_object($this->result)) {
            $rows[$i] = $row;
            $i++;
        }
        return $rows;
    }

}