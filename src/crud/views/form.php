<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="public/assets/ico/favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="public/css/dashboard.css" rel="stylesheet">

</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Devel Test</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" onclick="document.form.submit()">Salvar</a></li>
                <li><a href="index.php">Voltar</a></li>
            </ul>
        </div>

    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 main">
            <h1 class="page-header">Item</h1>

            <?php if (isset($_SESSION['msg'])): ?>
                <div class="alert alert-info"><?php echo $_SESSION['msg'] ?></div>
                <?php unset($_SESSION['msg']); endif; ?>

            <div class="container">

                <form class="form-signin" role="form" id="form" name="form" method="post" action="index.php?action=save">

                    <input type="hidden" name="id" value="<?php echo $item->id ?>">

                    <label>Titulo</label>
                    <input type="text" class="form-control" placeholder="Titulo" name="title" value="<?php echo $item->title ?>" required autofocus>
                    <br/>
                    <label>Slug</label>
                    <input type="text" class="form-control" placeholder="Slug" name="slug" value="<?php echo $item->slug ?>" required autofocus>
                    <br/>
                    <label>Descrição</label>
                    <input type="text" class="form-control" placeholder="Descrição" name="description" value="<?php echo $item->description ?>" required autofocus>
                    <br/>
                    <label>Conteudo</label>
                    <textarea name="body" rows="6" class="form-control"><?php echo $item->body ?></textarea>
                    <br/>
                    <label>Autor</label>
                    <input type="text" class="form-control" placeholder="Autor" name="author" value="<?php echo $item->author ?>" required autofocus>

                </form>

            </div>

        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="public/js/bootstrap.min.js"></script>
</body>
</html>
