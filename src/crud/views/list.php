<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="public/assets/ico/favicon.ico">

    <title>Dashboard Template for Bootstrap</title>

    <!-- Bootstrap core CSS -->
    <link href="public/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="public/css/dashboard.css" rel="stylesheet">

</head>

<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="index.php">Devel Test</a>
        </div>

        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="index.php?action=add">Adicionar Item</a></li>
            </ul>
        </div>


    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 main">
            <h1 class="page-header">Cadastros</h1>

            <?php if (isset($_SESSION['msg'])): ?>
                <div class="alert alert-info"><?php echo $_SESSION['msg'] ?></div>
                <?php unset($_SESSION['msg']); endif; ?>

            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Titulo</th>
                        <th>Author</th>
                        <th>Data de Inserir</th>
                        <th>Data de Atualização</th>
                        <th>Ação</th>
                    </tr>
                    </thead>
                    <tbody>

                    <?php foreach ($itens as $item): ?>
                        <tr>
                            <td><?php echo $item->id ?></td>
                            <td><?php echo $item->title ?></td>
                            <td><?php echo $item->author ?></td>
                            <td><?php echo $item->insert_date ?></td>
                            <td><?php echo $item->update_date ?></td>
                            <td>
                                <a href="index.php?action=delete&id=<?php echo $item->id ?>" class="delete glyphicon glyphicon-trash"></a>
                                <a href="index.php?action=edit&id=<?php echo $item->id ?>" class="glyphicon glyphicon-pencil"></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="public/js/bootstrap.min.js"></script>

<script type="text/javascript">
    jQuery(function () {
        jQuery('.delete').on('click', function (event) {
            if (confirm('Deseja realmente deletar?'))
                return true;
            else
                return false;
        });
    });
</script>
</body>
</html>
