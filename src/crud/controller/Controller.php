<?php
// no direct access
defined('_EXEC') or die('Acesso Restrito');

require_once PATH_ROOT . DS . 'model' . DS . 'Model.php';

class Controller
{

    /**
     * Listagem
     *
     * @return string
     */
    public function listAction()
    {
        ob_start();

        $model = new Model;
        $itens = $model->getAll();
        require PATH_ROOT . DS . 'views' . DS . 'list.php';

        $content = ob_get_contents();
        ob_clean();
        return $content;
    }

    /**
     * Edição
     *
     * @return string
     */
    public function editAction()
    {
        ob_start();

        $model = new Model;
        $item = $model->getItem($_REQUEST['id']);
        require PATH_ROOT . DS . 'views' . DS . 'form.php';

        $content = ob_get_contents();
        ob_clean();
        return $content;
    }

    /**
     * Salva Item
     */
    public function saveAction()
    {
        $model = new Model;
        if ($model->saveItem($_POST))
            $_SESSION['msg'] = 'Salvo com sucesso!';
        else
            $_SESSION['msg'] = 'Erro ao Salvar!';

        header('Location: ' . $_SERVER['PHP_SELF']);
    }

    /**
     * Adiciona Item
     */
    public function addAction()
    {
        ob_start();

        $item = new stdClass;
        $item->id = 0;
        $item->title = null;
        $item->slug = null;
        $item->description = null;
        $item->body = null;
        $item->author = null;

        require PATH_ROOT . DS . 'views' . DS . 'form.php';

        $content = ob_get_contents();
        ob_clean();
        return $content;
    }

    /**
     * Deleta item
     */
    public function deleteAction()
    {
        $model = new Model;
        $item = $model->getItem($_REQUEST['id']);
        if (!$item)
            $_SESSION['msg'] = 'Item não existe!';

        if ($model->delete($item->id))
            $_SESSION['msg'] = 'Deleta com sucesso!';
        else
            $_SESSION['msg'] = 'Erro ao Deletar!';

        header('Location: ' . $_SERVER['PHP_SELF']);
    }

}